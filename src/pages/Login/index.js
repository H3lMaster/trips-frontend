import React, { useState } from 'react';
import api from '../../services/api';


export default function Login({ history }) {


    const  [email, setEmail] = useState('');

    async function handleSubmit(event) {
      event.preventDefault();
      console.log(email);
  
      const response = await api.post('/sessions', { email});
      
      const { _id } = response.data;
  
      localStorage.setItem('user', _id);

      history.push('/dashboard');
    }
  
    function handleEmailChange(event) {
      setEmail(event.target.value)
    }

    return (  
    <>
      <p>
        Ofereça <strong>spots</strong> para programadores e encontre talentos.
      </p>
      <form onSubmit={handleSubmit}>
        <label htmlFor="email">Email *:</label>
        <input 
          type="email" 
          id="email" 
          placeholder="Seu email"
          value={email}
          onChange={handleEmailChange} />
          <button className="btn" type="submit">Entrar</button>
      </form>
      </>
    );
}