import React, { useState, useMemo } from 'react';
import api from '../../services/api';
import './styles.css';

import camera from '../../assets/camera.svg';

export default function New({ history }) {

    const [company, setCompany] = useState('');
    const [techs, setTechs] = useState('');
    const [price, setPrice] = useState('');
    const [thumbnail, setThumbnail] = useState(null);
    
    const preview = useMemo(
        () => { 
            return thumbnail ? URL.createObjectURL(thumbnail) : null;
        },
        [thumbnail]
    ); 

    async function handleSubmit(event) {
        event.preventDefault();

        const user_id = localStorage.getItem('user');
        const data = new FormData();
        data.append('thumbnail', thumbnail);
        data.append('techs', techs);
        data.append('price', price);
        data.append('company', company);

        await api.post('/spots', data, {
            headers: { user_id }
        });

        history.push('/dashboard');
    }

    return (
        <form onSubmit={handleSubmit}>
            <label 
                id="thumbnail" 
                style={{backgroundImage: `url(${preview})`}}
                className={thumbnail ? 'has-thumbnail' : ''}
            >
                <input type="file" onChange={event => setThumbnail(event.target.files[0])}/>
                <img src={camera} alt="select an image"/>    
            </label>
            <label htmlFor="company">Company *</label>
            <input 
                id="company"
                placeholder="Your company"
                value={company}
                onChange={event => setCompany(event.target.value)}
            />

            <label htmlFor="techs">Techs *<span>(comma separated)</span></label>
            <input 
                id="techs"
                placeholder="Your techs"
                value={techs}
                onChange={event => setTechs(event.target.value)}
            />

            <label htmlFor="price">Daily Price></label>      
            <input 
                id="price"
                placeholder="Price"
                value={price}
                onChange={event => setPrice(event.target.value)}
            />

            <button type="submit" className="btn">Create</button>
        </form>
    )
}